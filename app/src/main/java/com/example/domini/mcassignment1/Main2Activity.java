package com.example.domini.mcassignment1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mText = (TextView) findViewById(R.id.text_view);

        Bundle extras = getIntent().getExtras();
        String filteredPhrase = extras.getString("consonant");

        int[] consonantLetters = countConsonantLetters(filteredPhrase);

        mText.setText(
                "b = " + consonantLetters[0] + "\n" +
                        "c = " + consonantLetters[1] + "\n" +
                        "d = " + consonantLetters[2] + "\n" +
                        "f = " + consonantLetters[3] + "\n" +
                        "g = " + consonantLetters[4] + "\n" +
                        "h = " + consonantLetters[5] + "\n" +
                        "j = " + consonantLetters[6] + "\n" +
                        "k = " + consonantLetters[7] + "\n" +
                        "l = " + consonantLetters[8] + "\n" +
                        "m = " + consonantLetters[9] + "\n" +
                        "n = " + consonantLetters[10] + "\n" +
                        "p = " + consonantLetters[11] + "\n" +
                        "q = " + consonantLetters[12] + "\n" +
                        "r = " + consonantLetters[13] + "\n" +
                        "s = " + consonantLetters[14] + "\n" +
                        "t = " + consonantLetters[15] + "\n" +
                        "v = " + consonantLetters[16] + "\n" +
                        "w = " + consonantLetters[17] + "\n" +
                        "x = " + consonantLetters[18] + "\n" +
                        "y = " + consonantLetters[19] + "\n" +
                        "z = " + consonantLetters[20]
        );
    }

    int[] countConsonantLetters(String str) {
        str = str.toLowerCase();
        int[] letterCount = new int[21];

        for (int i = 0; i < str.length(); i++) {
            char chr = str.charAt(i);

            switch (chr) {
                case 'b': letterCount[0]++; break;
                case 'c': letterCount[1]++; break;
                case 'd': letterCount[2]++; break;
                case 'f': letterCount[3]++; break;
                case 'g': letterCount[4]++; break;
                case 'h': letterCount[5]++; break;
                case 'j': letterCount[6]++; break;
                case 'k': letterCount[7]++; break;
                case 'l': letterCount[8]++; break;
                case 'm': letterCount[9]++; break;
                case 'n': letterCount[10]++; break;
                case 'p': letterCount[11]++; break;
                case 'q': letterCount[12]++; break;
                case 'r': letterCount[13]++; break;
                case 's': letterCount[14]++; break;
                case 't': letterCount[15]++; break;
                case 'v': letterCount[16]++; break;
                case 'w': letterCount[17]++; break;
                case 'x': letterCount[18]++; break;
                case 'y': letterCount[19]++; break;
                case 'z': letterCount[20]++;
            }
        }

        return letterCount;
    }
}
