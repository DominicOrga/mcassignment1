package com.example.domini.mcassignment1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mPhraseText;
    private Button mInterpretButton;
    private EditText mVowelCountText;
    private EditText mFilteredPhraseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPhraseText = (EditText) findViewById(R.id.phrase_text);
        mInterpretButton = (Button) findViewById(R.id.interpret_button);
        mVowelCountText = (EditText) findViewById(R.id.vowel_count_text);
        mFilteredPhraseText = (EditText) findViewById(R.id.filtered_phrase_text);

        setTextFilters(30);
        setInterpretButtonListener();
    }

    private void setTextFilters(int limit) {
        mPhraseText.setFilters(new InputFilter[] { new InputFilter.LengthFilter((limit)) });
    }

    private void setInterpretButtonListener() {
        mInterpretButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phraseStr = mPhraseText.getText().toString();

                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

                intent.putExtra("consonant", phraseStr);

                startActivity(intent);
            }
        });
    }
}
